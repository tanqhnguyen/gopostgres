module gitlab.com/tanqhnguyen/gopostgres

go 1.14

require (
	github.com/go-pg/pg/v9 v9.1.6
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/tanqhnguyen/gologger v0.1.2
)
