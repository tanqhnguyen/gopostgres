package gopostgres

import (
	"fmt"
	"log"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/file"
)

// NewPostgresMigrationUsingEnvironment returns a PostgresMigration struct using the current config
func NewPostgresMigrationUsingEnvironment(migrationDir string) *PostgresMigration {
	fmt.Println("DEPRECATED! Use NewPostgresMigrationFromEnvironment instead")
	return NewPostgresMigrationFromEnvironment(migrationDir)
}

// NewPostgresMigrationFromEnvironment returns a PostgresMigration struct using the env variables
func NewPostgresMigrationFromEnvironment(migrationDir string) *PostgresMigration {
	pgConfig, err := LoadPostgresConfigFromEnv()

	if err != nil {
		panic(err)
	}

	return NewPostgresMigration(GeneratePostgresConnectionString(pgConfig), migrationDir)
}

// NewPostgresMigration returns a PostgresMigration struct
func NewPostgresMigration(connectionStr string, migrationDir string) *PostgresMigration {
	fileSource, err := (&file.File{}).Open(migrationDir)
	if err != nil {
		panic(err)
	}

	dbSource, err := (&postgres.Postgres{}).Open(connectionStr)
	if err != nil {
		panic(err)
	}

	m, err := migrate.NewWithInstance("file", fileSource, "postgres", dbSource)

	if err != nil {
		panic(err)
	}

	return &PostgresMigration{
		Migrate: m,
	}
}

// PostgresMigration manages postgres database schema
type PostgresMigration struct {
	Migrate *migrate.Migrate
}

// Force forcefully funs a version
func (m *PostgresMigration) Force(version int) error {
	log.Println("Running migrations...")

	err := m.Migrate.Force(version)
	if err != nil {
		return err
	}

	return nil
}

// Up runs all migration up to the latest one
func (m *PostgresMigration) Up() error {
	log.Println("Running migrations...")

	err := m.Migrate.Up()
	if err != nil && err != migrate.ErrNoChange {
		return err
	}

	log.Println("Everything is up to date")
	return nil
}

// Down reverts the schema by 1
func (m *PostgresMigration) Down() error {
	log.Println("Reverting one migration")

	return m.Migrate.Steps(-1)
}
