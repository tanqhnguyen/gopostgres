package gopostgres

import (
	"fmt"
	"strconv"

	"github.com/go-pg/pg/v9"
	"github.com/kelseyhightower/envconfig"
)

// PostgresCofig is the configuration of a postgresql server
type PostgresCofig struct {
	Database string `envconfig:"database" required:"true"`
	Username string `envconfig:"username" required:"true"`
	Password string `envconfig:"password" required:"true"`
	Host     string `envconfig:"host" default:"127.0.0.1"`
	Port     int    `envconfig:"port" default:"5432"`
}

// GeneratePostgresConnectionString constructs the connection string based on the config struct
func GeneratePostgresConnectionString(pgConfig PostgresCofig) string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		pgConfig.Username,
		pgConfig.Password,
		pgConfig.Host,
		strconv.Itoa(pgConfig.Port),
		pgConfig.Database,
	)
}

// LoadPostgresConfigFromEnv return the rabbitmq config as set in the environment variable
func LoadPostgresConfigFromEnv() (PostgresCofig, error) {
	var fromEnv PostgresCofig
	err := envconfig.Process("POSTGRES", &fromEnv)

	if err != nil {
		return PostgresCofig{}, err
	}

	return fromEnv, nil
}

// NewPostgresConnectionFromEnvironment uses env variables to connect to postgres
func NewPostgresConnectionFromEnvironment() *pg.DB {
	pgConfig, err := LoadPostgresConfigFromEnv()

	if err != nil {
		panic(err)
	}

	db := pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%d", pgConfig.Host, pgConfig.Port),
		User:     pgConfig.Username,
		Password: pgConfig.Password,
		Database: pgConfig.Database,
	})

	return db
}
