package gopostgres

import (
	"fmt"
	"time"

	"github.com/go-pg/pg/v9"
	"gitlab.com/tanqhnguyen/gologger"

	"github.com/stretchr/testify/suite"
)

// NewDatabaseTestSuite returns a new DatabaseTestSuite
func NewDatabaseTestSuite(name, envLocation, migrationDir string) *DatabaseTestSuite {
	return &DatabaseTestSuite{
		Name:         name,
		MigrationDir: migrationDir,
		Logger:       gologger.NewLogrusLoggerFromEnvironment(),
	}
}

// DatabaseTestSuite covers tests that need database connection
type DatabaseTestSuite struct {
	suite.Suite
	DB           *pg.DB
	Name         string
	EnvLocation  string
	MigrationDir string
	Logger       gologger.Logger
}

// CreateDatabase creates a new test database
func (s *DatabaseTestSuite) CreateDatabase(name string) {
	db := NewPostgresConnectionFromEnvironment()
	defer db.Close()

	var datname string
	_, err := db.QueryOne(pg.Scan(&datname), fmt.Sprintf("SELECT datname FROM pg_database WHERE datname = '%s'", name))

	if err == pg.ErrNoRows {
		_, err = db.Exec(fmt.Sprintf(`CREATE DATABASE "%s";`, name))
		if err != nil {
			panic(err)
		}
	} else if err != nil {
		panic(err)
	}
}

// RunMigrationOnDatabase runs migrations specifically on a database
func (s *DatabaseTestSuite) RunMigrationOnDatabase(name, dir string) {
	pgConfig, err := LoadPostgresConfigFromEnv()

	if err != nil {
		panic(err)
	}

	pgConfig.Database = name

	migration := NewPostgresMigration(GeneratePostgresConnectionString(pgConfig), dir)

	err = migration.Up()

	if err != nil {
		panic(err)
	}
}

// TruncateData truncates all tables in the provided schema
func (s *DatabaseTestSuite) TruncateData(schema string) {
	_, err := s.DB.Exec(`
	DO
	$$
	DECLARE
		l_stmt text;
	BEGIN
		SELECT 'truncate ' || string_agg(format('%I.%I', schemaname, tablename), ',')
			INTO l_stmt
		FROM pg_tables
		WHERE schemaname in (?);

		EXECUTE l_stmt;
	END;
	$$
	`, schema)

	if err != nil {
		panic(err)
	}
}

// SetupSuite creates a new database and runs migration
func (s *DatabaseTestSuite) SetupSuite() {
	start := time.Now()
	if s.Name == "" {
		panic("must set a name for the test suite")
	}

	s.CreateDatabase(s.Name)

	migrationDir := "migrations"
	if s.MigrationDir != "" {
		migrationDir = s.MigrationDir
	}
	s.RunMigrationOnDatabase(s.Name, migrationDir)

	pgConfig, _ := LoadPostgresConfigFromEnv()
	s.DB = pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%d", pgConfig.Host, pgConfig.Port),
		User:     pgConfig.Username,
		Password: pgConfig.Password,
		Database: s.Name,
	})
	elapsed := time.Since(start)
	fmt.Printf("Setting up suite took [%s]\n", elapsed)
}
